<!DOCTYPE html>
<html>
  <head>
    <title>Ejercicio 1 PHP</title>
  </head>
  <body>
    <p>Cree una sentencia if else que, en función del valor de una variable creada, imprima una u
otra cadena de texto. Compruebe su correcto funcionamiento para distinto valores de la variable.
    </p>

    <?php
    $a="Soy la cadena 1";
    $b="Soy la cadena 2";
    $c = 89;

    if($c<40){
        echo"$a";
    } else {
        echo"$b";
    }
    ?>
  </body>
</html>|