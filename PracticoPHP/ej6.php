<!DOCTYPE html>
<html>
  <head>
    <title>Ejercicio 6 PHP</title>
  </head>
  <body>
    <p>Generar un arreglo de 6 posiciones conteniendo el nombre de seis países con su respectiva moneda
(por ej: Argentina → Pesos Argentinos, Brasil → Real, USA → Dólar, etc). Se pide mostrar por
pantalla el contenido del arreglo.
</p>

    <?php
    $arreglo[6];
    $arreglo[0]="Argentina → Pesos Argentinos";
    $arreglo[1]="China → Yen";
    $arreglo[2]="Brasil → Real";
    $arreglo[3]="USA → Dólar";
    $arreglo[4]="Italia → Euro";
    $arreglo[5]="Chile → Peso Chileno";
    
    for($i=0;$i<6;$i++){
        echo "$arreglo[$i] <br/>";
    }

    ?>
  </body>
</html>|